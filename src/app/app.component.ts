import { Component } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  //providers: [AngularFirestore, AngularFireDatabase]
})
export class AppComponent { 
  name = '';
  
  title = 'Angular8Firebase';
  description = 'Angular-Fire-Demo';
  
  itemValue = '';
  members: Observable<any[]>;

  constructor(public db: AngularFireDatabase){
    this.members = db.list('members').valueChanges();
    console.log(this.members);
  }

  onSubmit(){
    //members as parent
    //content as info
    this.db.list('members').push({name: this.itemValue});
    this.itemValue = "";

  }
}
